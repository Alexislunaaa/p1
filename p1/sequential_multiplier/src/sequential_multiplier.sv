/***************************************************************************************************************
 
 This is the top module for the sequential multiplier implementation. The sub-modules instanciated
 for this top module are not only the multiplier itself, but also a bcd_decoder module to show it in 7-segments
 displays.
 
 
 
***************************************************************************************************************/
module sequential_multiplier
#(
	parameter DW = 8
)
(
	input 			start_in,
	input				clk,
	input				rst,
	input [DW-1:0] multiplier_in,
	input [DW-1:0] multiplicand_in,
	
	output [6:0] 	product_digit0_output,
	output [6:0] 	product_digit1_output,
	output [6:0] 	product_digit2_output,
	output [6:0] 	product_digit3_output,
	output [6:0] 	product_digit4_output,
	output [6:0] 	sign_output
);

/**************************************************************************************************
 WIRE ZONE
*****************************************************************************************************/
wire 		  			sign_w;			//This will be connected to the bit 15 of the product (sign bit)
wire					start_oneshot_w;

wire [DW-1:0]		multiplicand_sync_w;
wire [DW-1:0]		multiplier_sync_w;
/*This wire will transport the ouput of our multiplier (negative or positive where 
  the last bit will be the sign)*/
wire [(DW*2)-1:0] product_w;
wire					ready_w;
wire [(DW*2)-1:0] product_register_w;
wire [(DW*2)-1:0] product_complement_a2_w;
wire [(DW*2)-1:0] product_or_complemented_w;
/*These wires connect the output of the bcd_decoder module to the outputs of this main module,
  which will be connected directly to the 7-segs displays*/
wire [6:0] 			bcd_digit0_w;
wire [6:0] 			bcd_digit1_w;
wire [6:0] 			bcd_digit2_w;
wire [6:0] 			bcd_digit3_w;
wire [6:0] 			bcd_digit4_w;



/*****************************************************************************************************
INSTANCING AND CONNECTION ZONE
******************************************************************************************************/

//This module gives a one shot for the start signal to synchronize and give it a 1 clk cycle duration
OneShot
#(
	.DW(1)
)
start_oneshot
(
	.inp(start_in),
	.rst(rst),
	.clk(clk),
	
	.out(start_oneshot_w)
);



//This register syncronizes the data input
universal_register
#(
	.DW(DW*2),						
	.reg_type(2'b11)								//PIPO Register		
)
multiplicand_sync
(
	.clk(clk),
	.rst(rst),										//Resets when equal to 0
	.sync_rst(1'b1),								//Resets when equal to 0
	.en(1'b1),										//Controled by the system_on_o of the control module
	.l_s(1'b1),										//Not used for this type of register
	.data_in(multiplicand_in),							//Connected to the output of the multiplier	
	
	.data_out(multiplicand_sync_w)				//Connected to the bcd module
);	

//This register syncronizes the data input
universal_register
#(
	.DW(DW*2),						
	.reg_type(2'b11)								//PIPO Register		
)
multiplier_sync
(
	.clk(clk),
	.rst(rst),										//Resets when equal to 0
	.sync_rst(1'b1),								//Resets when equal to 0
	.en(1'b1),										//Controled by the system_on_o of the control module
	.l_s(1'b1),										//Not used for this type of register
	.data_in(multiplier_in),					//Connected to the output of the multiplier	
	
	.data_out(multiplier_sync_w)				//Connected to the bcd module
);	


//This module is the multiplier itself
seq_multiplier
#(
	.DW(DW)
)
multiplier 
(
	.multiplicand_i(multiplicand_sync_w),
	.multiplier_i(multiplier_sync_w),
	.start_i(start_oneshot_w),
	.clk_i(clk),
	.rst_i(rst),
	
	.product_o(product_w),
	.ready_o(ready_w)
);

//This register saves the result when ready
universal_register
#(
	.DW(DW*2),						
	.reg_type(2'b11)								//PIPO Register		
)
product_register
(
	.clk(clk),
	.rst(rst),										//Resets when equal to 0
	.sync_rst(1'b1),								//Resets when equal to 0
	.en(ready_w),									//Controled by the system_on_o of the control module
	.l_s(1'b1),										//Not used for this type of register
	.data_in(product_w),							//Connected to the output of the multiplier	
	
	.data_out(product_register_w)				//Connected to the bcd module
);	


complemento_a_2#(
	.DW(DW*2)
) bcd_complement(
	.inp(product_register_w),
	.out(product_complement_a2_w)
);



mux_2_to_1#(
.	DW(DW*2)
)mux_product_or_complemented(
	.inp_a(product_register_w),
	.inp_b(product_complement_a2_w),
	.selector(product_register_w[(DW*2)-1]),
	.out(product_or_complemented_w)
);


/*After calculating the product, it is decoded with the BCD and the 
  output would be compatible with the 7-segments display*/
bcd_decoder
#(
	.NBits(15)								//This parameter is 15 because the 16th bit is used the sign-bit
)
bcd_main
(
	.bcd_decoder_in(product_or_complemented_w[(DW*2)-2:0]),	//The input is the 16-bit product minus the sign-bit
	 
	.bcd_digit0_out(bcd_digit0_w),	//The 7-bit outputs are connected to the 5 7-seg-displays
	.bcd_digit1_out(bcd_digit1_w),
	.bcd_digit2_out(bcd_digit2_w),
	.bcd_digit3_out(bcd_digit3_w),
	.bcd_digit4_out(bcd_digit4_w)
);

assign sign_w = ~(product_register_w[(DW*2)-1]);
 
//The output of bcd_main will be connected directly to the 7-segs display pins
assign product_digit0_output = bcd_digit0_w;
assign product_digit1_output = bcd_digit1_w;
assign product_digit2_output = bcd_digit2_w;
assign product_digit3_output = bcd_digit3_w;
assign product_digit4_output = bcd_digit4_w;
assign sign_output			  = {sign_w,6'b111111};


endmodule
