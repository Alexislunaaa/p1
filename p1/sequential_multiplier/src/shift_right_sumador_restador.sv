/********************************************************************************
Author: Neidys Vargas 
v1.0 Created in: Sep 2023
********************************************************************************/
module shift_right_sumador_restador
#(
	parameter DW = 8
)
(
	input bit 		clk,
	input bit 		rst,							
	input bit 		sync_rst,					
	input bit 		en,							
	input logic		l_s,					
	input logic 	[DW-1:0] data_in,			
	
	output logic	[DW-1:0] data_result_out

);

wire [DW-1:0] 	data_in_w;
logic [DW-1:0] shift_result_l;


always_ff@(posedge clk or negedge rst)begin

        if(!rst)begin
		  
            shift_result_l <= '0;
				
        end
		  
        else begin
		  
            if(en)begin
				

                if(l_s) begin
					 
                    shift_result_l <= data_in_w;
                end
					 
                else begin
                    
                    shift_result_l <= ({shift_result_l[DW-1], shift_result_l[DW-1:1]}) & {DW{sync_rst}};                
                end
                
            end
				
				else begin
				
					shift_result_l <= shift_result_l & {DW{sync_rst}};  
					
				end
				
        end
end


assign data_in_w = data_in & {DW{sync_rst}};	
assign data_result_out = shift_result_l;

endmodule 
