/*********************************************************************************************
  Author: Alejandro Porras, Alexis Luna and Neidys Vargas
 v1.0 Created: February 2023
 v1.1 Edited:  September 2023
 
**********************************************************************************************/
module seq_multiplier
#(
	parameter DW = 8
) 
(
	input [DW-1:0] 		multiplicand_i,
	input [DW-1:0] 		multiplier_i,
	input						start_i,
	input						clk_i,
	input						rst_i,
	
	output [(DW*2)-1:0] 	product_o,
	output					ready_o
);

/*********************************************************************************************
 Wire zone
**********************************************************************************************/

wire first_cycle_w;
wire system_on_w;
wire operation_w;

wire [(DW*2)-1:0] multiplicand_extended_w;


wire [(DW*2)-1:0] multiplicand_or_zero_w;

wire [DW-1:0] 		shifted_multiplier_w;
wire [(DW*2)-1:0] shifted_multiplicand_w;

wire [(DW*2)-1:0] adder_substractor_result_w;

wire 					dummy_wire_1;
wire					dummy_wire_2;
wire [(DW*2)-1:0] accumulator_output_w;

wire ready_w /* synthesis keep */;

/****************************************************************************************************
 INSTANCING ZONE
*****************************************************************************************************/
//Control module
seq_multiplier_control
#(
	.DW(8)
)
control
(
	.start_i(start_i),
	.sign_i(multiplier_i[DW-1]),
	.clk_i(clk_i),
	
	.ready_o(ready_w),						//The ready signal of the result
	.first_cycle_o(first_cycle_w),		//Used as selector for the muxes
	.system_on_o(system_on_w),				//Enable for all the modules of the Sequential Multiplier
	.operation_o(operation_w)				//Used to select the operation in the Adder/substractor
);


mux_2_to_1
#(
	.DW(DW*2)
)
mux_multiplicand_or_Z
(
	.inp_a('0),			
	.inp_b(shifted_multiplicand_w),			
	.selector(shifted_multiplier_w[DW-1]),
	
	.out(multiplicand_or_zero_w)								
);

//Shifter for the multiplier
shift_left_sumador_restador
#(
	.DW(DW)
)
multiplier_shift
(
	.clk(clk_i),
	.rst(rst_i),				
	.sync_rst(1'b1),				
	.en(system_on_w),			
	.l_s(first_cycle_w),		
	.data_in(multiplier_i),				
	
	.data_result_out(shifted_multiplier_w)
);

//Shifter for the multiplicand
shift_right_sumador_restador
#(
	.DW(DW*2)
)
multiplicand_shift
(
	.clk(clk_i),
	.rst(rst_i),				
	.sync_rst(1'b1),			
	.en(system_on_w),			
	.l_s(first_cycle_w),		
	.data_in(multiplicand_extended_w),				
	
	.data_result_out(shifted_multiplicand_w)
);


sumador_restador
#(
	.DW(DW*2)						
)
add_sub 
(
	.dato1_in(multiplicand_or_zero_w),
	.dato2_in(accumulator_output_w),
	.option_in(operation_w),
	
	.result_sumador_out({dummy_wire_2, adder_substractor_result_w})
);

//The accumulator
universal_register
#(
	.DW(DW*2),						
	.reg_type(2'b11)								//PIPO Register		
)
accumulator
(
	.clk(clk_i),
	.rst(rst_i),									//Resets when equal to 0
	.sync_rst(~first_cycle_w),									//Resets when equal to 0
	.en(system_on_w),								//Controled by the system_on_o of the control module
	.l_s(1'b1),								//Not used for this type of register
	.data_in(adder_substractor_result_w),	//Connected to the output of the adder_substractor	
	
	.data_out(accumulator_output_w)			//Connected to the output of the multiplier and to the input of the adder
);	

//Equal to Multiplicand << DW-1
assign multiplicand_extended_w = {multiplicand_i[DW-1], multiplicand_i[DW-1:0], {(DW-1){'0}}};
assign ready_o = ready_w;
assign product_o = accumulator_output_w;
endmodule

