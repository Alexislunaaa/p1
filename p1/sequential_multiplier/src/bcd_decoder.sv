/*********************************************************************************************
 Author: Alejandro Porras
 V1.0 Created: January 2023
 
 This is the main module of the BCD decoder to instance all the submodules
**********************************************************************************************/

module bcd_decoder
#(
	parameter NBits = 8
)
(
	input [NBits-1:0] bcd_decoder_in,	//A 3-digit decimal number of 8 bits (255 max)
	output [6:0]  bcd_digit0_out,			//The units digit of the introduced 3-digit number, connected to the 7 segment
	output [6:0]  bcd_digit1_out,			//The tens digit of the introduced 3-digit number, connected to the 7 segment
	output [6:0]  bcd_digit2_out,			//The hundreds digit of the introduced 3-digit number, connected to the 7 segment
	output [6:0]  bcd_digit3_out,			//The thousands digit of the introduced 3-digit number, connected to the 7 segment
	output [6:0]  bcd_digit4_out			//The tens of thousands digit of the introduced 3-digit number, connected to the 7 segment
);


/*********************************************************************************************
 Wire zone
**********************************************************************************************/

wire [19:0] bcd_step1_w;
wire [19:0] bcd_step2_w;
wire [19:0] bcd_step3_w;
wire [19:0] bcd_step4_w;
wire [19:0] bcd_step5_w;
wire [19:0] bcd_step6_w;
wire [19:0] bcd_step7_w;
wire [19:0] bcd_step8_w;
wire [19:0] bcd_step9_w;
wire [19:0] bcd_step10_w;
wire [19:0] bcd_step11_w;
wire [19:0] bcd_step12_w;
wire [19:0] bcd_step13_w;
wire [19:0] bcd_step14_w;
wire [19:0] bcd_step15_w;

wire [6:0] digit0_output_w;
wire [6:0] digit1_output_w;
wire [6:0] digit2_output_w;
wire [6:0] digit3_output_w;
wire [6:0] digit4_output_w;
/****************************************************************************************************
 INSTANCING ZONE
*****************************************************************************************************/

//Step 1
bcd_step
#(
	.NBits(NBits)
)
step1
(
	.bcd_in({19'b0000_0000_0000_0000_000,bcd_decoder_in[NBits-1]}),
	.bcd_out(bcd_step1_w)
);

//Step 2
bcd_step
#(
	.NBits(NBits)
)
step2
(
	.bcd_in({bcd_step1_w[18:0], bcd_decoder_in[NBits-2]}),
	.bcd_out(bcd_step2_w)
);

//Step 3
bcd_step
#(
	.NBits(NBits)
)
step3
(
	.bcd_in({bcd_step2_w[18:0], bcd_decoder_in[NBits-3]}),
	.bcd_out(bcd_step3_w)
);

//Step 4
bcd_step
#(
	.NBits(NBits)
)
step4
(
	.bcd_in({bcd_step3_w[18:0], bcd_decoder_in[NBits-4]}),
	.bcd_out(bcd_step4_w)
);

//Step 5
bcd_step
#(
	.NBits(NBits)
)
step5
(
	.bcd_in({bcd_step4_w[18:0], bcd_decoder_in[NBits-5]}),
	.bcd_out(bcd_step5_w)
);

//Step 6
bcd_step
#(
	.NBits(NBits)
)
step6
(
	.bcd_in({bcd_step5_w[18:0], bcd_decoder_in[NBits-6]}),
	.bcd_out(bcd_step6_w)
);

//Step 7
bcd_step
#(
	.NBits(NBits)
)
step7
(
	.bcd_in({bcd_step6_w[18:0], bcd_decoder_in[NBits-7]}),
	.bcd_out(bcd_step7_w)
);

//Step 8
bcd_step
#(
	.NBits(NBits)
)
step8
(
	.bcd_in({bcd_step7_w[18:0], bcd_decoder_in[NBits-8]}),
	.bcd_out(bcd_step8_w)
);

//Step 9
bcd_step
#(
	.NBits(NBits)
)
step9
(
	.bcd_in({bcd_step8_w[18:0], bcd_decoder_in[NBits-9]}),
	.bcd_out(bcd_step9_w)
);

//Step 10
bcd_step
#(
	.NBits(NBits)
)
step10
(
	.bcd_in({bcd_step9_w[18:0], bcd_decoder_in[NBits-10]}),
	.bcd_out(bcd_step10_w)
);

//Step 11
bcd_step
#(
	.NBits(NBits)
)
step11
(
	.bcd_in({bcd_step10_w[18:0], bcd_decoder_in[NBits-11]}),
	.bcd_out(bcd_step11_w)
);

//Step 12
bcd_step
#(
	.NBits(NBits)
)
step12
(
	.bcd_in({bcd_step11_w[18:0], bcd_decoder_in[NBits-12]}),
	.bcd_out(bcd_step12_w)
);

//Step 13
bcd_step
#(
	.NBits(NBits)
)
step13
(
	.bcd_in({bcd_step12_w[18:0], bcd_decoder_in[NBits-13]}),
	.bcd_out(bcd_step13_w)
);

//Step 14
bcd_step
#(
	.NBits(NBits)
)
step14
(
	.bcd_in({bcd_step13_w[18:0], bcd_decoder_in[NBits-14]}),
	.bcd_out(bcd_step14_w)
);


//Convert nibble 0
nibble_to_7_segments
digit_0
(
	.nibble_in({bcd_step14_w[2:0],bcd_decoder_in[0]}),
	.segments_out(digit0_output_w)
);

//Convert nibble 1
nibble_to_7_segments
digit_1
(
	.nibble_in(bcd_step14_w[6:3]),
	.segments_out(digit1_output_w)
);

//Convert nibble 2
nibble_to_7_segments
digit_2
(
	.nibble_in(bcd_step14_w[10:7]),
	.segments_out(digit2_output_w)
);

//Convert nibble 3
nibble_to_7_segments
digit_3
(
	.nibble_in(bcd_step14_w[14:11]),
	.segments_out(digit3_output_w)
);

//Convert nibble 4
nibble_to_7_segments
digit_4
(
	.nibble_in(bcd_step14_w[18:15]),
	.segments_out(digit4_output_w)
);

assign bcd_digit0_out = digit0_output_w;
assign bcd_digit1_out = digit1_output_w;
assign bcd_digit2_out = digit2_output_w;
assign bcd_digit3_out = digit3_output_w;
assign bcd_digit4_out = digit4_output_w;

endmodule
