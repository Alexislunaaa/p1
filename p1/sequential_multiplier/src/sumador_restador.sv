/********************************************************************************
Author: Neidys Vargas 
v1.0 Created in: Sep 2023
********************************************************************************/
module sumador_restador
#(
	parameter DW = 8
) 
(
	input [DW-1:0] dato1_in,
	input [DW-1:0] dato2_in,
	input 			option_in,
	
	output [DW:0]  result_sumador_out
);


logic [DW:0] result_sumador_out_l;


always_comb begin
	if (option_in == 0)
		result_sumador_out_l = dato1_in + dato2_in;
	else
		result_sumador_out_l = dato2_in - dato1_in;
end

assign result_sumador_out = result_sumador_out_l;

endmodule
		