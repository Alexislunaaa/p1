//Coder:          Alexis Luna Delgado
//Description:    This module defines the two complement
module complemento_a_2#(
parameter DW = 8
) (
input [DW-1:0]inp,
output [DW-1:0]out
);

logic [(DW)-1:0]complement;

always_comb begin
		complement = (~inp)+1'b1;
end

assign out = complement; 

endmodule
