/*************************************************************************************************************
Author: Alejandro Porras
 v1.0 Created in: February 2023
 v1.1 Created in: September 2023
 
 Notes of v1.1: added more documentation and removed unnecessary signals.
 
 Inputs:
	start_i: signal to start the multiplication after introducing the multiplicand and multiplier. It shall
				be connected to a button.
	sign_i:	receives the most significant bit of the multiplier, determining its sign, and therefore, the
				operation to be performed by the adder_substractor (addition when MSB == 0, or substraction when
				MSB == 1).
	clk_i:	the clock signal of the system.
	
 Outputs:
	ready_o:	the signal that activates when the multiplication has been completed. Connected directly to an
				output of the sequential multiplier.
	first_cycle_o: activated only during the first cycle of the multiplication, right when the start_i has been 
						activated.
	system_on_o: 	activated when the multiplication starts and deactivated when the multiplication ends. It
						works as an enable for registers of the sequential multiplier.
	operation_o:	selects the operation to be performed by the Adder_Substractor, its value is directly
						determined by the sign_i.
 
This module is part of the seq_multiplier module. This has very specific functions focused on controlling
the sequential multiplier to have a proper functioning, mainly generating control signals for the other modules.
*************************************************************************************************************/

module seq_multiplier_control
#(
	parameter DW = 8
)
(
	input 	start_i,
	input 	sign_i,
	input 	clk_i,
	
	output	ready_o,				
	output	first_cycle_o,		
	output	system_on_o,		
	output	operation_o			
	
);

logic [3:0] cycle_counter_l;	//reimplement to use an external counter
logic			system_active_l;
logic			ready_l;
logic			operation_l;

always_ff@(posedge clk_i or posedge start_i) begin
	//In this state, the operation had just begun
	if (start_i == 1) begin
		system_active_l 	<= 1'b1;
		cycle_counter_l 	<= 1'b0;
		ready_l				<= 1'b0;
		operation_l			<= sign_i;
	end
	else begin
		//In this state, the operation is in progress
		if (system_active_l == 1)
			cycle_counter_l 	<= cycle_counter_l + 1'b1;
		//In this state, the operation has ended
		if (cycle_counter_l == DW-1) begin
			system_active_l 	<= 1'b0;
			ready_l 				<= 1'b1;
		end	
		operation_l   <= 1'b0;
		
	end

end


assign system_on_o 	= system_active_l;
assign operation_o 	= operation_l;
assign first_cycle_o = start_i;
assign ready_o 		= ready_l;
	
endmodule

