/*************************************************************************************************************
Author: Alejandro Porras
V1.0 Created: January 2023
V1.1 Updated: August 2023

V1.1 notes: updated for correct use of sync_rst.

This module is a universal register that can work either as a SISO, PIPO, PISO or SIPO, depending
on the way it is instanced.
The configuration of the module is the following:
	reg_type == 2'b00: SISO MSB first
		*Only the DW-1 bit of the data_in must be used to receive data and the DW-1 bit of the data_out to send.
	reg_type == 2'b01: SIPO MSB first
		*Only the DW-1 bit of the data_in must be used to receive data and all bits of the data_out to send.
	reg_type == 2'b10: PISO MSB first
		*All bits of data_in are used but only the DW-1 bit will be used for the data_out.
	reg_type == 2'b11: PIPO
		*All bits of the data_in and data_out will be used. The highest bit is the MSB.
*************************************************************************************************************/

module universal_register
#(
	parameter 		DW = 4,						//4-bit register by default
	parameter 		reg_type = 2'b11			//PIPO by default
)
(
	input bit 		clk,
	input bit 		rst,							//Resets when equal to 0
	input bit 		sync_rst,					//Resets when equal to 0
	input bit 		en,							//The register only works when en == 1
	input logic		l_s,							//Used only for the PISO register (1 to load and 0 to shift)
	input logic 	[DW-1:0] data_in,			
	
	output logic	[DW-1:0] data_out
);	


//Local parameters used to identify every mode
localparam		SISO = 2'b00;
localparam		SIPO = 2'b01;
localparam 		PISO = 2'b10;
localparam 		PIPO = 2'b11;



//This will save the values depending on the size of the register
logic [DW-1:0] register_r;			//This will save the data of sec_register every posedge in the sequential process
logic [DW-1:0] sec_register_r; 	//This will save the data of the input in the combinational process






//Combinational process
always_comb begin: COMB_BLOCK
	//SISO
	if(reg_type == SISO)
		sec_register_r = {register_r[DW-2:0], data_in[DW-1]} & {DW{sync_rst}}; //Shift what's already saved and apply sync_rst
	//SIPO
	else if(reg_type == SIPO)
		sec_register_r = {register_r[DW-2:0], data_in[DW-1]} & {DW{sync_rst}}; //Shift what's already saved and apply sync_rst
	//PISO
	else if(reg_type == PISO) 
	begin
		if(l_s)														//Load when l_s == 1
			sec_register_r = data_in & {DW{sync_rst}};	//Load all the bits at the data_in
		else															//Shift when l_s == 0
			sec_register_r = {register_r[DW-2:0], register_r[DW-1]} & {DW{sync_rst}}; //Shift what's already saved 
	end
	//PIPO
	else 		//Make a PIPO in any other case as default
		sec_register_r = data_in & {DW{sync_rst}};	//Use all the bits of the input
		
end: COMB_BLOCK


//Generate the DW flip flops
always_ff@(posedge clk or negedge rst) begin: univ_reg
	if(rst == 0)
		register_r <= '0;						//Reset the register
	else if (en)
		register_r <= sec_register_r;		//Load the processed value depending on the reg_type into the register.
		
end: univ_reg

assign data_out = register_r;

endmodule





