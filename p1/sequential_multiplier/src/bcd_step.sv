/*********************************************************************************************
 Author: Alejandro Porras
 v1.0 Created in: January 2023
 v1.1 Created in: August 2023
 
 Notes of v1.1: updated to use always_comb instead of always
 
 Description of module: This module represents one step of the bcd process
**********************************************************************************************/


module bcd_step
#(
	parameter NBits = 16
)
(
	input [19:0] bcd_in,			//One nibble is needed for every digit of the decimal number to be represented
	output [19:0] bcd_out
);

logic [19:0] bcd_out_l;
//Add 3 to any nibble that is >= 5
always_comb
begin
	//Nibble 0
	if (bcd_in[3:0] >= 4'b0101)
		bcd_out_l[3:0] = bcd_in[3:0] + 4'b0011;
	else
		bcd_out_l[3:0] = bcd_in[3:0];
	//Nibble 1
	if (bcd_in[7:4] >= 4'b0101)
		bcd_out_l[7:4] = bcd_in[7:4] + 4'b0011;
	else
		bcd_out_l[7:4] = bcd_in[7:4];
	//Nibble 2
	if (bcd_in[11:8] >= 4'b0101)
		bcd_out_l[11:8] = bcd_in[11:8] + 4'b0011;
	else
		bcd_out_l[11:8] = bcd_in[11:8];
	//Nibble 3
	if (bcd_in[15:12] >= 4'b0101)
		bcd_out_l[15:12] = bcd_in[15:12] + 4'b0011;
	else
		bcd_out_l[15:12] = bcd_in[15:12];
	//Nibble 4
	if (bcd_in[19:16] >= 4'b0101)
		bcd_out_l[19:16] = bcd_in[19:16] + 4'b0011;
	else
		bcd_out_l[19:16] = bcd_in[19:16];
end

assign bcd_out = bcd_out_l;

endmodule 
