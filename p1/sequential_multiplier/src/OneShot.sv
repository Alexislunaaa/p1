//Coder:          Alexis Luna Delgado
//Description:    This module defines a one shot implementation to sync an asincronous inputs while a 1 clock cicle 

module OneShot
#(
	parameter DW = 1
)
(
	input [DW-1:0]  inp,
	input 			 rst,
	input				 clk,
	
	output [DW-1:0] out
);


wire [DW-1:0] first_reg_output_w;
wire [DW-1:0] second_reg_output_w;


PIPO_reg#(
	.DW(DW)
) 
first_PIPO_register(
	.clk(clk),
	.rst(rst),
	.enb(1'b1),
	.inp(inp),
	.out(first_reg_output_w)
);

PIPO_reg#(
.DW(DW)
) 
second_PIPO_register(
	.clk(clk),
	.rst(rst),
	.enb(1'b1),
	.inp(first_reg_output_w),
	.out(second_reg_output_w)
);


assign out = first_reg_output_w & ~second_reg_output_w;

endmodule 
