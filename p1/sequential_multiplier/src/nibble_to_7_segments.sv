/***********************************************************************************************

 Author: Alejandro Porras
 v1.0 Created: January 2023
 v1.1 Created in: August 2023
 
 Notes of v1.1: updated to use always_comb instead of always
 
 This module is used to convert a 4-bit number into a 7-bit number usable for the 7 seg- display
 This is only applicable for the Altera Cyclone IV as it is directly implemented according to 
 its pins of the 7-segment display. Refer its manual to see the configuration.
 Inputs:
	nibble_in: 4-bit input which is the number we desire to show.
 Outputs:
	segments_out: 7-bit output which is adequated according to the reference manual. This should
	be connected directly to the 7-segs display pins.
************************************************************************************************/

module nibble_to_7_segments
(
	input [3:0] nibble_in,
	output [6:0] segments_out
);

logic [6:0] result;

always_comb
begin
	case(nibble_in)
		0:begin
			result = 7'b100_0000; //Bit 6 off
		end
		1:begin
			result = 7'b111_1001; //Bits 1 and 2 on
		end
		2:begin
			result = 7'b010_0100; //Bits 5 and 2 off
		end
		3:begin
			result = 7'b011_0000; //Bits 5 and 4 off
		end
		4:begin
			result = 7'b001_1001; //Bits 0, 3 and 4 off
		end
		5:begin
			result = 7'b001_0010; //Bits 1 and 4 off
		end
		6:begin
			result = 7'b000_0010; //Bit 1 off
		end
		7:begin
			result = 7'b111_1000; //Bits 0,1 and 2 on
		end
		8:begin
			result = 7'b000_0000; //All bits on
		end
		9:begin
			result = 7'b001_0000; //Bit 4 off
		end
		default: begin
			result = 7'b111_1111; //All bits off
		end
	endcase
end

assign segments_out = result;

endmodule
