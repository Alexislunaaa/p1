/********************************************************************************
Author: Neidys Vargas 
v1.0 Created in: Sep 2023
********************************************************************************/
module shift_left_sumador_restador

#(
	parameter DW = 8
)
(
	input 			clk,
	input 			rst,							//Normal Reset
	input 			sync_rst,					//Sync Reset
	input 			en,							
	input logic		l_s,							
	input logic 	[DW-1:0] data_in,			
	
	output logic	[DW-1:0] data_result_out

);
//This will be used for the sync reset
wire  [DW-1:0] data_in_w;
logic [DW-1:0] shift_result_l;


always_ff@(posedge clk or negedge rst)begin

        if(!rst)begin
		  
            shift_result_l <= '0;
				
        end
		  
        else begin
		  
            if(en)begin

                if(l_s) begin                 // if l_s is equal to 1, load the data into the shift_result
					 
                    shift_result_l <= data_in_w;
                end
					 
                else begin
                     
                    shift_result_l <= ({shift_result_l[DW-2:0], 1'b0}) & {DW{sync_rst}};   
                
					 end
                
            end
				
				else begin
				
					shift_result_l <= shift_result_l & {DW{sync_rst}};   
					
				end
        end
end

assign data_in_w = data_in & {DW{sync_rst}};	
assign data_result_out = shift_result_l;

endmodule 
