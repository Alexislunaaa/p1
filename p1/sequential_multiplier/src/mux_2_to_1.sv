//Coder:          Alexis Luna Delgado
//Description:    This module defines a multiplexor two to one 


module mux_2_to_1
#(parameter DW= 8)
(
input [DW-1:0]inp_a,
input [DW-1:0]inp_b,
input selector,
output [DW-1:0]out
);

logic [DW-1:0]result_o;

always_comb begin

	case(selector)
		1'b1:result_o = inp_b;
		1'b0:result_o = inp_a;
		default:result_o = 0;
	endcase
end
assign out=result_o;

endmodule
