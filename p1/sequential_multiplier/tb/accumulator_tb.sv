/********************************************************************************
 Author: Alejandro Porras
 v1.0 Created: February 2023

This is the testbench file for the accumulator module
********************************************************************************/

`timescale 1ns / 1ps
module accumulator_tb;

localparam DW_tb = 16;
localparam reg_type_tb = 2'b11;			//PIPO by default
 
logic clk_tb;
logic rst_tb;
logic sync_rst_tb;
logic en_tb;
logic ls_tb;
logic [DW_tb-1:0] data_i_tb;

logic [DW_tb-1:0] data_o_tb;


universal_register
#(
	.DW(DW_tb),
	.reg_type(reg_type_tb)
)
accumulator
(
	.clk(clk_tb),
	.rst(rst_tb),							//Resets when equal to 0
	.sync_rst(sync_rst_tb),					//Resets when equal to 0
	.en(en_tb),							//The register only works when en == 1
	.l_s(ls_tb),							//1 to load and 0 to shift
	.data_in(data_i_tb),			
	
	.data_out(data_o_tb)

);

initial begin	 
	#2
            clk_tb     	= 0;
            rst_tb		= 0;
				sync_rst_tb	= 0;
				en_tb 		= 0;
				ls_tb			= 0;
				data_i_tb	= '0;
	 
    #2     
				rst_tb		= 1;
				sync_rst_tb	= 1;
				data_i_tb	= 16'b1010_1111_0000_0101;
				en_tb  		= 1;
	 #2
				data_i_tb   = 16'b1111_1111_1111_1111;
				en_tb			= 0;
    #2
				sync_rst_tb	= 0;
				data_i_tb 	= 1;
	 
	 #100
    $stop;
	 
end

	
	
	
always begin
	 #1 clk_tb <= ~clk_tb;
end	
	
endmodule
