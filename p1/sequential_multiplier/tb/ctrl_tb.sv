/********************************************************************************
Author: Alejandro Porras
 v1.0 Created in: February 2023

This is the testbench file for the control module of the seq_multiplier
********************************************************************************/

`timescale 1ns / 1ps
module seq_multiplier_ctrl_tb;

localparam DW_tb = 8; 

logic clk_tb;
logic start_i_tb;
logic sign_i_tb;

logic ready_o_tb;
logic first_cycle_o_tb;
logic system_on_o_tb;
logic operation_o_tb;
logic [3:0] cycle_counter_test_o_tb;

seq_multiplier_control
#(
	.DW(DW_tb)
)
ctrl_tb
(
	.start_i(start_i_tb),
	.sign_i(sign_i_tb),
	.clk_i(clk_tb),
	
	.ready_o(ready_o_tb),				//The ready signal of the result
	.first_cycle_o(first_cycle_o_tb),		//Used as selector for the muxes
	.system_on_o(system_on_o_tb),		//Enable for all the modules of the Sequential Multiplier
	.operation_o(operation_o_tb),			//Used to select the operation in the Adder/substractor
	.cycle_counter_test_o(cycle_counter_test_o_tb)
);

initial begin	 

            clk_tb     	= 0;
            start_i_tb	= 0;
				sign_i_tb	= 0;
	 
    #3      start_i_tb	= 1;
				sign_i_tb	= 1;
	 #2		start_i_tb  = 0;
	 
    #20
				start_i_tb	= 1;
				sign_i_tb	= 0;
	 #2		start_i_tb  = 0;
	 
	 #100
    $stop;
	 
end

	
	
	
always begin
	 #1 clk_tb <= ~clk_tb;
end	
	
endmodule
