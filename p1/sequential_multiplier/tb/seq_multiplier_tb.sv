
/********************************************************************************
Author: Alejandro Porras
v1.0 Created in: February 2023

This is the testbench file for the control module of the seq_multiplier
********************************************************************************/

`timescale 1ns / 1ps
module seq_multiplier_tb;

localparam DW_tb = 8; 

logic 					clk_i_tb;
logic 					start_i_tb;
logic 					rst_i_tb;
logic [DW_tb-1:0] 		multiplicand_i_tb;
logic [DW_tb-1:0] 		multiplier_i_tb;

logic 					ready_o_tb;
logic [(DW_tb*2)-1:0] 	product_o_tb;

seq_multiplier
#(
	.DW(DW_tb)
)
multiplier_tb 
(
	.multiplicand_i(multiplicand_i_tb),
	.multiplier_i(multiplier_i_tb),
	.start_i(start_i_tb),
	.clk_i(clk_i_tb),
	.rst_i(rst_i_tb),
	
	.product_o(product_o_tb),
	.ready_o(ready_o_tb)
);

initial begin	 

            clk_i_tb   	= 0;
            start_i_tb	= 0;
				rst_i_tb		= 0;
	 
    #3      
				rst_i_tb 			= 1;
				multiplicand_i_tb = 8'b0000_0101;//5
				multiplier_i_tb	= 8'b0000_0111;//7
				start_i_tb			= 1;		
	 #2		
				start_i_tb  = 0; // This should be the effect of the one-shot for the start signal
				
	 #18      
				rst_i_tb 			= 1;
				multiplicand_i_tb = 8'b1111_1111;//-1
				multiplier_i_tb	= 8'b1111_1111;//-1
				start_i_tb			= 1;
	 #2		
				start_i_tb  = 0;
				
				
	 #18      
				rst_i_tb 			= 1;
				multiplicand_i_tb = 8'b1111_1110;//-2
				multiplier_i_tb	= 8'b0111_1111;//127
				start_i_tb			= 1;
	 #2		
				start_i_tb  = 0;
	 
	 
	 #18      
				rst_i_tb 			= 1;
				multiplicand_i_tb = 8'b0000_0111;//7
				multiplier_i_tb	= 8'b1111_1100;//-4
				start_i_tb			= 1;
	 #2		
				start_i_tb  = 0;
				
	 #18      
				rst_i_tb 			= 1;
				multiplicand_i_tb = 8'b0111_1111;//127
				multiplier_i_tb	= 8'b0000_0010;//2
				start_i_tb			= 1;
	 #2		
				start_i_tb  = 0;
	 
	 #100
    $stop;
	 
end

	
	
	
always begin
	 #1 clk_i_tb <= ~clk_i_tb;
end	
	
endmodule
