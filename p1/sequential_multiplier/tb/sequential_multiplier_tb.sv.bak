/********************************************************************************
Author: Alejandro Porras
v1.0 Created in: February 2023

This is the testbench file for the control module of the seq_multiplier
********************************************************************************/

`timescale 1ns / 1ps
module sequential_multiplier_tb;

localparam DW_tb = 8; 

logic 					clk_i_tb;
logic 					start_i_tb;
logic 					rst_i_tb;
logic [DW_tb-1:0] 	multiplicand_i_tb;
logic [DW_tb-1:0] 	multiplier_i_tb;

logic [6:0] 	product_digit0_output_tb;
logic [6:0] 	product_digit1_output_tb;
logic [6:0] 	product_digit2_output_tb;
logic [6:0] 	product_digit3_output_tb;
logic [6:0] 	product_digit4_output_tb;
logic [6:0] 	sign_output_tb;


sequential_multiplier
#(
	.DW(DW_tb)
)
sequential_multiplier_tb 
(
	.multiplicand_i(multiplicand_i_tb),
	.multiplier_i(multiplier_i_tb),
	.start_i(start_i_tb),
	.clk_i(clk_i_tb),
	.rst_i(rst_i_tb),
	
	.product_digit0_output(product_digit0_output_tb),
	.product_digit1_output(product_digit1_output_tb),
	.product_digit2_output(product_digit2_output_tb),
	.product_digit3_output(product_digit3_output_tb),
	.product_digit4_output(product_digit4_output_tb),
	.sign_output(sign_output_tb)
);

initial begin	 

            clk_i_tb   	= 0;
            start_i_tb	= 0;
				rst_i_tb		= 0;
	 
    #3      
				rst_i_tb 			= 1;
				multiplicand_i_tb = 8'b0000_0101;//5
				multiplier_i_tb	= 8'b0000_0111;//7
	 #2			
				start_i_tb			= 1;		
	 #2		
				start_i_tb  = 0; // This should be the effect of the one-shot for the start signal
				
	 #18      
				rst_i_tb 			= 1;
				multiplicand_i_tb = 8'b1111_1111;//-1
				multiplier_i_tb	= 8'b1111_1111;//-1
	 #2			
				start_i_tb			= 1;
	 #2		
				start_i_tb  = 0;
				
				
	 #18      
				rst_i_tb 			= 1;
				multiplicand_i_tb = 8'b1111_1110;//-2
				multiplier_i_tb	= 8'b0111_1111;//127
	 #2			
				start_i_tb			= 1;
	 #2		
				start_i_tb  = 0;
	 
	 
	 #18      
				rst_i_tb 			= 1;
				multiplicand_i_tb = 8'b0000_0111;//7
				multiplier_i_tb	= 8'b1111_1100;//-4
	 #2			
				start_i_tb			= 1;
	 #2		
				start_i_tb  = 0;
				
	 #18      
				rst_i_tb 			= 1;
				multiplicand_i_tb = 8'b0111_1111;//127
				multiplier_i_tb	= 8'b0000_0010;//2
	 #2			
				start_i_tb			= 1;
	 #2		
				start_i_tb  = 0;
	 
	 #100
    $stop;
	 
end

	
	
	
always begin
	 #1 clk_i_tb <= ~clk_i_tb;
end	
	
endmodule
